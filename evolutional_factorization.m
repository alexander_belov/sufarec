function u = evolutional_factorization( ic, tau )
% Solves parabolic problem via evolutional factorized scheme with a given
% set of time steps.
global mu; global grid_choice;
u1 = ic; dim = size(ic);
if( length(dim) == 2 )
    N = dim(1) - 2; K = dim(2) - 2; NKL = max(N,K);
    A = zeros(1,NKL+1); B = A; C = A; F = A;
    v = zeros(N+2,K+2); step = v;
    if (grid_choice == 0)
        [x,hx,y,hy] = adaptive_mesh( N,K );
    else
        [x,hx,y,hy] = user_mesh( N,K );
    end
    kx = zeros(N+1,K+1); ky = kx; kappa = kx;
    for n = 1:N+1
        for k = 1:K+1
            x_temp = 0.5*(x(n+1)+x(n)); y_temp = 0.5*(y(k+1)+y(k));
            [kx(n,k),~] = coefficients(x_temp,y(k));
            [~,ky(n,k)] = coefficients(x(n),y_temp);
            [~,~,~,kappa(n,k)] = coefficients(x(n),y(k));
        end
    end
    for m = 1:length(tau)
        for k = 2:K+1
            for n = 2:N+1
                Ax   = 2*(mu^2)*kx(n-1,k)/hx(n-1)/(hx(n)+hx(n-1));
                Bx   = 2*(mu^2)*kx( n ,k)/hx( n )/(hx(n)+hx(n-1));
                Cx   = - (Ax + Bx);
                A(n) = - 0.5*tau(m)*Ax; B(n) = - 0.5*tau(m)*Bx;
                C(n) = - 1 + 0.5*tau(m)*Cx - (tau(m)/4)*kappa(n,k);            

                Ay   = 2*(mu^2)*ky(n,k-1)/hy(k-1)/(hy(k)+hy(k-1));
                By   = 2*(mu^2)*ky( n ,k)/hy( k )/(hy(k)+hy(k-1));
                Cy   = - (Ay + By);
                F1   = - Ax*u1(n-1,k) - Cx*u1(n,k) - Bx*u1(n+1,k);
                F2   = - Ay*u1(n,k-1) - Cy*u1(n,k) - By*u1(n,k+1);
                F(n) = - right_hand(x(n),y(k)) + kappa(n,k)*u1(n,k)+F1+F2;
            end
            v(:,k) = tridiagonal_matrix_algorithm(A,B,C,F,N);
        end
        for n = 2:N+1
            for k=2:K+1
                Ay  = 2*(mu^2)*ky(n,k-1)/hy(k-1)/(hy(k)+hy(k-1));
                By  = 2*(mu^2)*ky(n, k )/hy( k )/(hy(k)+hy(k-1));
                Cy  = - (Ay + By);
                A(k) = - 0.5*tau(m)*Ay;  B(k) = - 0.5*tau(m)*By;
                C(k) = - 1 + 0.5*tau(m)*Cy - (tau(m)/4)*kappa(n,k);
                F(k) = - v(n,k);
            end
            step(n,:) = tridiagonal_matrix_algorithm(A,B,C,F,K);
        end
        u = u1 + tau(m)*step;
        u1 = u;
    end
end
if( length(dim) == 3 )
    N = dim(1) - 2; K = dim(2) - 2; L = dim(3) - 2; NKL = max( [N,K,L] );
    A = zeros(1,NKL+1); B = A; C = A; F = A;
    w = zeros(N+2,K+2,L+2); v = w; step = w;
    if (grid_choice == 0)
        [x,hx,y,hy,z,hz] = adaptive_mesh( N,K,L );
    else
        [x,hx,y,hy,z,hz] = user_mesh( N,K,L );
    end
    kx = zeros(N+1,K+1,L+1); ky = kx; kz = kx; kappa = kx;
    for n = 1:N+1
        for k = 1:K+1
            for l = 1:L+1
                x_temp = 0.5*(x(n+1)+x(n));
                y_temp = 0.5*(y(k+1)+y(k));
                z_temp = 0.5*(z(l+1)+z(l));
                [kx(n,k,l),~,~] = coefficients(x_temp,y(k),z(l));
                [~,ky(n,k,l),~] = coefficients(x(n),y_temp,z(l));
                [~,~,kz(n,k,l)] = coefficients(x(n),y(k),z_temp);
                [~,~,~,kappa(n,k,l)] = coefficients(x(n),y(k),z(l));
            end
        end
    end
    for m = 1:length(tau)
        for n = 2:N+1 
            for k = 2:K+1
                for l = 2:L+1
                    Az = 2*(mu^2)*kz(n,k,l-1)/hz(l-1)/(hz(l)+hz(l-1));
                    Bz = 2*(mu^2)*kz(n,k, l )/hz( l )/(hz(l)+hz(l-1));
                    Cz = - (Az + Bz);
                    A(l) = - 0.5*tau(m)*Az;  B(l) = - 0.5*tau(m)*Bz;
                    C(l) = - 1 + 0.5*tau(m)*Cz - (tau(m)/6)*kappa(n,k,l);
                    
                    Ax = 2*(mu^2)*kx(n-1,k,l)/hx(n-1)/(hx(n)+hx(n-1));
                    Bx = 2*(mu^2)*kx(n,k, l )/hx( n )/(hx(n)+hx(n-1));
                    Cx = - (Ax + Bx);
                    Ay = 2*(mu^2)*ky(n,k-1,l)/hy(k-1)/(hy(k)+hy(k-1));
                    By = 2*(mu^2)*ky(n, k ,l)/hy( k )/(hy(k)+hy(k-1));
                    Cy = - (Ay + By);
                    F1 = - Ax*u1(n-1,k,l) - Cx*u1(n,k,l) - Bx*u1(n+1,k,l);
                    F2 = - Ay*u1(n,k-1,l) - Cy*u1(n,k,l) - By*u1(n,k+1,l);
                    F3 = - Az*u1(n,k,l-1) - Cz*u1(n,k,l) - Bz*u1(n,k,l+1);
                    F(l) = - right_hand(x(n),y(k),z(l))...
                        + kappa(n,k,l)*u1(n,k,l) + F1 + F2 + F3;
                end
                w(n,k,:) = tridiagonal_matrix_algorithm(A,B,C,F,L);
            end
        end
        for n = 2:N+1
            for l = 2:L+1
                for k=2:K+1
                    Ay  = 2*(mu^2)*ky(n,k-1,l)/hy(k-1)/(hy(k)+hy(k-1));
                    By  = 2*(mu^2)*ky(n, k ,l)/hy( k )/(hy(k)+hy(k-1));
                    Cy  = - (Ay + By);
                    A(k) = - 0.5*tau(m)*Ay;  B(k) = - 0.5*tau(m)*By;
                    C(k) = - 1 + 0.5*tau(m)*Cy - (tau(m)/6)*kappa(n,k,l);
                    F(k) = - w(n,k,l);
                end
                v(n,:,l) = tridiagonal_matrix_algorithm(A,B,C,F,K);
            end
        end
        for k=2:K+1
            for l = 2:L+1
                for n = 2:N+1
                    Ax = 2*(mu^2)*kx(n-1,k,l)/hx(n-1)/(hx(n)+hx(n-1));
                    Bx = 2*(mu^2)*kx( n ,k,l)/hx( n )/(hx(n)+hx(n-1));
                    Cx = - (Ax + Bx);
                    A(n) = - 0.5*tau(m)*Ax;  B(n) = - 0.5*tau(m)*Bx;
                    C(n) = - 1 + 0.5*tau(m)*Cx - (tau(m)/6)*kappa(n,k,l);
                    F(n)  = - v(n,k,l);
                end
                step(:,k,l) = tridiagonal_matrix_algorithm(A,B,C,F,N);
            end
        end
        u = u1 + tau(m)*step;
        u1 = u;
    end
end