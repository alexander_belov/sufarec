global example;
example = 3;
if(example == 2) % 2D example, mandatory arguments only
    Mu = 1e-2; % small parameter
    ax = -1; bx = 1; ay = -1; by = 1; % domain boundaries
    Boundaries = [ax,bx,ay,by];
    [u, epsilon_final, iteration_precision] = SuFaReC(Mu, Boundaries);
end
if( example == 3 ) % 3D example, all arguments
    Mu = 1e-2; % small parameter
    Grid_choice = 0; % grid type
    epsilon_grid_sys = 1e-4; % accuracy of the grid system solving
    epsilon_pde  = 1e-2; % accuracy of the PDE solving
    Norm_choice = 0; % mesh norm type
    ax = -1; bx = 1; ay = -1; by = 1; az = -1; bz = 1; % domain boundaries
    Boundaries = [ax,bx,ay,by,az,bz];
    [u, epsilon_final, iteration_precision] = SuFaReC...
        (Mu, Boundaries,epsilon_pde,Grid_choice,epsilon_grid_sys,...
        Norm_choice);
end