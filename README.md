MATLAB package for solving Dirichlet problem for generalized Helmholz equation (including singularly perturbed one) in rectangular domain.

The package is run by the "SuFaReC" function (see "TEST.m" for an example). Detailed comments on the work of the functions are given in the m-files.