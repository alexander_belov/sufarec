function [x,hx,y,hy,z,hz] = user_mesh( N,K,L )
% User-defined semi-uniform mesh generating functions.
x_user = @(x)( x ); x_user_deriv = @(x)( 1 );
y_user = @(x)( x ); y_user_deriv = @(x)( 1 );
z_user = @(x)( x ); z_user_deriv = @(x)( 1 );

global boundaries;
ax = boundaries(1); bx = boundaries(2);
ay = boundaries(3); by = boundaries(4);
xi1 = -1:2/(N+1):1; xi1_semi = -N/(N+1):2/(N+1):N/(N+1);
xi2 = -1:2/(K+1):1; xi2_semi = -K/(K+1):2/(K+1):K/(K+1);
x = zeros(1,N+2); y = zeros(1,K+2); hx = zeros(1,N+1); hy = zeros(1,K+1);
for n = 1:N+2
    x(n) = 0.5*(bx+ax) + 0.5*(bx-ax)*x_user(xi1(n));
end
for k = 1:K+2
    y(k) = 0.5*(by+ay) + 0.5*(by-ay)*y_user(xi2(k));
end
for n = 1:N+1
    hx(n) = 0.5*(bx-ax)*x_user_deriv( xi1_semi(n) )/(N+1);
end
for k = 1:K+1
    hy(k) = 0.5*(by-ay)*y_user_deriv( xi2_semi(k) )/(K+1);
end
if( nargin == 3 )
    az = boundaries(5); bz = boundaries(6);
    xi3 = -1:2/(L+1):1; xi3_semi = -L/(L+1):2/(L+1):L/(L+1);
    z = zeros(1,L+2); hz = zeros(1,L+1);
    for l = 1:L+2
        z = 0.5*(bz+az) + 0.5*(bz-az)*z_user(xi3(l));
    end
    for l = 1:L+1
        hz(l) = 0.5*(bz-az)*z_user_deriv( xi3_semi(l) )/(L+1);
    end
end
end