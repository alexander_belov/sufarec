function S_all = number_of_steps( lambda_max, lambda_min, epsilon )
% Calculates the numbers of steps in nested logarithmic grids via a priori
% convergence estimate.
relation = sum(lambda_max)/sum(lambda_min);
S = ceil(-( 4/(pi^2 + 2*pi))*log( relation )*log(epsilon));
S_temp = zeros(1,10); S_temp(1) = S; I = 1;
while (S_temp(I) >= 2)
    S_temp(I+1) = S_temp(I)/2;
    I = I+1; if (I == 10); break; end
end
S_all = zeros(1,I);
for m = 1:I
    S_temp(m) = ceil( S_temp(I) )*2^(m-1);
    S_all(m) = S_temp(m);
end
end