function [kx,ky,kz,kappa] = coefficients(x,y,z)
% Coefficients of the Helmholz equation.
global example
if(example == 2); kx = 2+y^2; ky = 2+x; kz = 0;     kappa = 1; end
if(example == 3); kx = 3-z;   ky = 2+x; kz = 1+y^2; kappa = 1; end
end