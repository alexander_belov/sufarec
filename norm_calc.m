function Norm = norm_calc(u)
% Calculates norm of a grid function.
global norm_choice; global grid_choice;
dim = size(u);
if( length(dim) == 2 )
    N = dim(1) - 2; K = dim(2) - 2;
    if (grid_choice == 0)
        [~,hx,~,hy] = adaptive_mesh( N,K );
    else
        [~,hx,~,hy] = user_mesh( N,K );
    end
    if (norm_choice == 0); Norm = max( max( abs(u) ) ); end
    if (norm_choice == 1)
        intx = zeros(1,N+2); Int1 = 0;
        for n = 1:N+2
            for k = 1:K+1
                intx(n) = intx(n) + 0.5*hy(k)*(u(n,k)^2 + u(n,k+1)^2);
            end
        end
        for n = 1:N+1
            Int1 = Int1 + 0.5*hx(n)*(intx(n) + intx(n+1));
        end
        Norm = sqrt(Int1);
    end
    if (norm_choice == 2)
        average = 0;
        for n = 1:N+2
            for k = 1:K+2
               average = average + u(n,k)^2;
            end
        end
        Norm = sqrt(average/(K+2)/(N+2));
    end
end
if( length(dim) == 3 )
    N = dim(1) - 2; K = dim(2) - 2; L = dim(3) - 2;
    if (grid_choice == 0)
        [~,hx,~,hy,~,hz] = adaptive_mesh( N,K,L );
    else
        [~,hx,~,hy,~,hz] = user_mesh( N,K,L );
    end
    if (norm_choice == 0); Norm = max(max(max(abs(u)))); end
    if (norm_choice == 1)
        intxy = zeros(N+2,K+2); intx = zeros(1,N+2); Int1 = 0;            
        for n = 1:N+2
            for k = 1:K+2
                for l = 1:L+1
                    intxy(n,k) = intxy(n,k)...
                        + 0.5*hz(l)*(u(n,k,l)^2 + u(n,k,l+1)^2);
                end
            end
        end
        for n = 1:N+2
            for k = 1:K+1
                intx(n) = intx(n)...
                    +0.5*hy(k)*(intxy(n,k)+intxy(n,k+1));
            end
        end
        for n = 1:N+1
            Int1 = Int1 + 0.5*hx(n)*(intx(n) + intx(n+1));
        end
        Norm = sqrt(Int1);
    end
    if (norm_choice == 2)
        average = 0;
        for n = 1:N+2
            for k = 1:K+2
                for l = 1:L+2
                    average = average + u(n,k,l)^2;
                end
            end
        end
        Norm = sqrt(average/(K+2)/(N+2)/(L+2));
    end
end
end