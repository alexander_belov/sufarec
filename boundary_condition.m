function bc = boundary_condition(x,y,z)
% Boundary conditions (Dirichlet type).
global example;
if(example == 2); bc = 2.5*(x+y); end
if(example == 3); bc = 2.5*(x+y+z); end
end