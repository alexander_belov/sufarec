function [solution, iteration_precision] = SFRC_solver(N, K, L)
% Solves grid system on a given grid via superfast relaxation count.
global grid_choice; global eps_grid_sys;
if( nargin == 2 )
    if (grid_choice == 0) 
        [x,~,y,~] = adaptive_mesh( N,K );
    else
        [x,~,y,~] = user_mesh( N,K );
    end
    [lambda_max, lambda_min, conditioning] = spectrum_estimates( N,K );
    epsilon_background = 10^(-16.2)*conditioning;
    epsilon = max(epsilon_background, eps_grid_sys);
    S_all = number_of_steps(lambda_max, lambda_min, epsilon);
    I = length(S_all); u_all = zeros(N+2,K+2,I); ic = zeros(N+2,K+2);
    for log_grid_num = 1:I
        S = S_all(log_grid_num);
        tau_lt = logarithmic_set(lambda_max, lambda_min, S);
        if( log_grid_num == 1 )
            tau = tau_lt;
            for k = 1:K+2
                ic( 1 ,k) = boundary_condition(x( 1 ), y(k));
                ic(N+2,k) = boundary_condition(x(N+2), y(k));
            end
            for n = 1:N+2
                ic(n, 1 ) = boundary_condition(x(n), y( 1 ));
                ic(n,K+2) = boundary_condition(x(n), y(K+2));
            end
        else
            tau = zeros(1,S/2);
            for s = 1:S/2
                tau(s) = tau_lt(2*s);
            end
            ic = u_all(:,:,log_grid_num-1);
        end
        u_all(:,:,log_grid_num) = evolutional_factorization(ic, tau);
    end
    solution = u_all(:,:,I);
end
if( nargin == 3 )
    if (grid_choice == 0)
        [x,~,y,~,z,~] = adaptive_mesh( N,K,L );
    else
        [x,~,y,~,z,~] = user_mesh( N,K,L );
    end
    [lambda_max, lambda_min, conditioning] = spectrum_estimates( N,K,L );
    epsilon_background = 10^(-16.2)*conditioning;
    epsilon = max(epsilon_background, eps_grid_sys);
    S_all = number_of_steps(lambda_max, lambda_min, epsilon);
    I = length(S_all);
    u_all = zeros(N+2,K+2,L+2,I); ic = zeros(N+2,K+2,L+2);
    for log_grid_num = 1:I
        S = S_all(log_grid_num);
        tau_lt = logarithmic_set(lambda_max, lambda_min, S);
        if( log_grid_num == 1 )
            tau = tau_lt;
            for k = 1:K+2
                for l = 1:L+2
                    ic( 1 ,k,l) = boundary_condition(x( 1 ), y(k), z(l));
                    ic(N+2,k,l) = boundary_condition(x(N+2), y(k), z(l));
                end
            end
            for n = 1:N+2
                for l = 1:L+2
                    ic(n, 1 ,l) = boundary_condition(x(n), y( 1 ), z(l));
                    ic(n,K+2,l) = boundary_condition(x(n), y(K+2), z(l));
                end
            end
            for n = 1:N+2
                for k = 1:K+2
                    ic(n,k, 1 ) = boundary_condition(x(n), y(k), z( 1 ));
                    ic(n,k,L+2) = boundary_condition(x(n), y(k), z(L+2));
                end
            end
        else
            tau = zeros(1,S/2);
            for s = 1:S/2
                tau(s) = tau_lt(2*s);
            end
            ic = u_all(:,:,:,log_grid_num-1);
        end
        u_all(:,:,:,log_grid_num) = evolutional_factorization(ic, tau);
    end
    solution = u_all(:,:,:,I);
end
[precision_i, precision_e] = ...
    iteration_precision_estimates(u_all, epsilon_background);
iter_conv_illustration...
    (S_all, precision_i, precision_e, epsilon_background);
if (I==1 || I==2); iteration_precision = epsilon; end
if (I>=3); iteration_precision = precision_e(I-2); end 
end