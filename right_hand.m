function f = right_hand(x,y,z)
% Right hand of the equation.
global example;
if( example == 2 ); f = cos(0.25*pi*(x+y)^2)*cos(0.75*pi*(y-x)); end
if( example == 3 ); f = 1.5*cos( pi*(z+1)*(x^2+y) )^2*cos( pi*(x+z) ); end
end