function iter_conv_illustration...
    (S_all, precision_i, precision_e, epsilon_background)
% Performs relaxation count convergence plots in semilogarithmic scale.
global eps_grid_sys; epsilon = max(epsilon_background, eps_grid_sys);
I = length(S_all); S_intr = zeros(1,I-1); S_extr = zeros(1,I-2);
for log_grid_num = 1:I-1
    S_intr(log_grid_num) = S_all(log_grid_num);
end
for log_grid_num = 1:I-2
    S_extr(log_grid_num) = S_all(log_grid_num+2);
end
figure; xlabel('S'); ylabel('lg Norm'); title('ITERATION PRECISION');
if (I == 1); hold on; plot(S_all(I),log10(epsilon),...
        'rs','LineWidth',2,'MarkerEdgeColor','k','MarkerSize',11)
end
if (I >= 2); hold on;
    plot(S_intr,log10(precision_i),...
        'o' ,'LineWidth',2,'MarkerEdgeColor','k','MarkerSize',11)
end
if (I == 2); hold on; plot(S_all(I), log10(epsilon),...
        'rs','LineWidth',2,'MarkerEdgeColor','k','MarkerSize',11)
end
if (I >= 3); hold on; plot(S_extr,log10(precision_e),...
        'rs','LineWidth',2,'MarkerEdgeColor','k','MarkerSize',14)
end
end