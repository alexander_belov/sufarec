function [u,epsilon_u,iteration_precision] = SuFaReC(Mu, Boundaries,...
    Epsilon_pde, Grid_choice, Epsilon_grid_sys, Norm_choice)
% Solves Helmholz equation with guaranteed accuracy. This function carried
% out calculation on a sequence of nested until accuracy determined by
% Richardson method is less than user-required accuracy or 1e-3 by default.
if(nargin == 6); default_parameters = 0; end
if( nargin < 6 && nargin > 2 )
    disp('Error! Given auxiliary parameters are incomplete!');
    default_parameters = ...
        input('Do you wish to select default ones? 1 -> yes, 0 -> no: ');
    if( default_parameters == 0 )
        disp('Calculation aborted.');
        u = 0; epsilon_u = 0; iteration_precision = 0;
        return;
    end
end
if(nargin == 2); default_parameters = 1; end
if(nargin < 2)
    disp('Error! Problem statement is incomplete!');
    u = 0; epsilon_u = 0; iteration_precision = 0;
    return;
end
if( default_parameters == 1 )
    Epsilon_pde = 1e-3; Epsilon_grid_sys = 1e-5;
    Grid_choice = 0;    Norm_choice = 0;
end

global mu; global boundaries; global grid_choice; 
global eps_grid_sys; global norm_choice;
mu = Mu; boundaries = Boundaries; eps_grid_sys = Epsilon_grid_sys;
norm_choice = Norm_choice; grid_choice = Grid_choice;
N0 = 10; K0 = 10; L0 = 10; max_grid_num = 6;
grid_precision = zeros(1,max_grid_num); 
iteration_precision = zeros(1,max_grid_num);
condition = 1; grid_num = 1;
while( condition )
    if( grid_num > max_grid_num ); break; end;
    if( grid_num == 1 )
        if( length(boundaries) == 4 )
            N = N0-1; K = K0-1;
            [u1,iteration_precision(grid_num)] = SFRC_solver( N,K );
        end
        if( length(boundaries) == 6 )
            N = N0-1; K = K0-1; L = L0-1;
            [u1,iteration_precision(grid_num)] = SFRC_solver( N,K,L );
        end
    else u1 = u2;
    end
    grid_num = grid_num + 1;
    if( length(boundaries) == 4 )
        N = (N+1)*2-1; K = (K+1)*2-1;   
        [u2,iteration_precision(grid_num)] = SFRC_solver( N,K );
    end
    if( length(boundaries) == 6 )
        N = (N+1)*2-1; K = (K+1)*2-1; L = (L+1)*2-1;   
        [u2,iteration_precision(grid_num)] = SFRC_solver( N,K,L );
    end
    p = 2;
    precision_u = richardson_multidim( u1, u2, p );
    grid_precision(grid_num-1) = norm_calc(precision_u);
    condition = grid_precision(grid_num-1) > Epsilon_pde; 
end
u = u2; actual_grid_num = grid_num;
precision_output = zeros(1,actual_grid_num - 1);
for m = 1:actual_grid_num - 1
    precision_output(m) = grid_precision(m);
end
epsilon_u = precision_output(actual_grid_num-1);
pde_solve_illustration(u, N0, K0, L0, precision_output);
end