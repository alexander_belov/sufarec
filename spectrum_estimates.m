function [lambda_max, lambda_min, conditioning] = spectrum_estimates( N,K,L )
% Constructs estimates for spectrum boundaries of the grid system.
global grid_choice; global mu; global boundaries;
if( nargin == 2 )
    est_x = zeros(N+1,K+1); est_y = est_x;
    kx = est_x; ky = est_x; kappa1 = est_x; kappa2 = est_x;
    if (grid_choice == 0)
        [x,hx,y,hy] = adaptive_mesh( N,K );
    else
        [x,hx,y,hy] = user_mesh( N,K );
    end
    for n = 1:N+1
        for k = 1:K+1
            x_s = 0.5*(x(n+1)+x(n)); y_s = 0.5*(y(k+1)+y(k));
            [kx(n,k),~,~,kappa1(n,k)] = coefficients(x_s,y(k));
            [~,ky(n,k),~,kappa2(n,k)] = coefficients(x(n),y_s);
        end
    end
    for n = 2:N+1
        for k = 2:K+1            
            k1 = kx(n-1,k); k2 = kx(n,k);
            est_x(n,k) = mu^2*4*(k1/hx(n-1) + k2/hx(n))/(hx(n)+hx(n-1))...
                + 0.5*kappa1(n,k);
            k3 = ky(n,k-1); k4 = ky(n,k);
            est_y(n,k) = mu^2*4*(k3/hy(k-1) + k4/hy(k))/(hy(k)+hy(k-1))...
                + 0.5*kappa2(n,k);
        end
    end
    lambda_x_max_est = max(max(est_x)); lambda_y_max_est = max(max(est_y));
    lambda_max = [lambda_x_max_est, lambda_y_max_est];   
    lx = boundaries(2) - boundaries(1); ly = boundaries(4) - boundaries(3);
    lambda_x_min_est = mu^2*(pi/lx)^2*min(min(kx + 0.5*kappa1));
    lambda_y_min_est = mu^2*(pi/ly)^2*min(min(ky + 0.5*kappa2));
    lambda_min = [lambda_x_min_est, lambda_y_min_est];
end
if( nargin == 3 )
    est_x = zeros(N+1,K+1,L+1); est_y = est_x; est_z = est_x;
    kx = est_x; ky = est_x; kz = est_x; 
    kappa1 = est_x; kappa2 = est_x; kappa3 = est_x;    
    if (grid_choice == 0)
        [x,hx,y,hy,z,hz] = adaptive_mesh( N,K,L );
    else
        [x,hx,y,hy,z,hz] = user_mesh( N,K,L );
    end
    for n = 1:N+1
        for k = 1:K+1
            for l = 1:L+1
                x_s = 0.5*(x(n+1)+x(n));
                y_s = 0.5*(y(k+1)+y(k));
                z_s = 0.5*(z(l+1)+z(l));
                [kx(n,k,l),~,~,kappa1(n,k,l)]= coefficients(x_s,y(k),z(l));
                [~,ky(n,k,l),~,kappa2(n,k,l)]= coefficients(x(n),y_s,z(l));
                [~,~,kz(n,k,l),kappa3(n,k,l)]= coefficients(x(n),y(k),z_s);                
            end
        end
    end
    for n = 2:N+1
        for k = 2:K+1
            for l = 2:L+1
                k1 = kx(n-1,k,l); k2 = kx(n,k,l);
                est_x(n,k,l) = mu^2*4*(k1/hx(n-1)...
                    + k2/hx(n))/(hx(n)+hx(n-1)) + kappa1(n,k,l)/3;
                k3 = ky(n,k-1,l); k4 = ky(n,k,l);
                est_y(n,k,l) = mu^2*4*(k3/hy(k-1)...
                    + k4/hy(k))/(hy(k)+hy(k-1)) + kappa2(n,k,l)/3;
                k5 = ky(n,k,l-1); k6 = ky(n,k,l);
                est_z(n,k,l) = mu^2*4*(k5/hz(l-1)...
                    + k6/hz(l))/(hz(l)+hz(l-1)) + kappa3(n,k,l)/3;
            end
        end
    end
    lambda_x_max_est = max(max(max(est_x)));
    lambda_y_max_est = max(max(max(est_y)));
    lambda_z_max_est = max(max(max(est_z))); 
    lambda_max = [lambda_x_max_est, lambda_y_max_est, lambda_z_max_est];
    lx = boundaries(2) - boundaries(1);
    ly = boundaries(4) - boundaries(3);
    lz = boundaries(6) - boundaries(5);
    lambda_x_min_est = mu^2*(pi/lx)^2*min(min(min( kx + kappa1/3 )));
    lambda_y_min_est = mu^2*(pi/ly)^2*min(min(min( ky + kappa2/3 )));
    lambda_z_min_est = mu^2*(pi/lz)^2*min(min(min( kz + kappa3/3 )));
    lambda_min = [lambda_x_min_est, lambda_y_min_est, lambda_z_min_est];
end
conditioning = sum(lambda_max)/sum(lambda_min);
end