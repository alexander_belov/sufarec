function [x,hx,y,hy,z,hz] = adaptive_mesh( N,K,L )
% Generates semi-uniform mesh adapted to boundary layer and tranzition
% zone. The mesh has N, K (and L in 3D) inner points.
global boundaries; global mu;
ax = boundaries(1); bx = boundaries(2);
ay = boundaries(3); by = boundaries(4);
if( nargin == 2 )
    kappa_av = zeros(nargin);
    x_un = ax:(bx-ax)/N:bx; y_un = ay:(by-ay)/K:by;
    for n = 1:length(x_un)
        for k = 1:length(y_un)
            [~,~,~,kappa_av(n,k)] = coefficients(x_un(n),y_un(k));
        end
    end
    kappa_av = sum(sum(kappa_av))/length(x_un)/length(y_un);
end
if( nargin == 3 )
    az = boundaries(5); bz = boundaries(6);
    kappa_av = zeros(nargin);
    x_un = ax:(bx-ax)/N:bx; y_un = ay:(by-ay)/K:by; z_un = az:(bz-az)/L:bz;
    for n = 1:length(x_un)
        for k = 1:length(y_un)
            for l = 1:length(z_un)
                [~,~,~,kappa_av(n,k,l)] = ...
                    coefficients(x_un(n),y_un(k),z_un(l));
            end
        end
    end
    kappa_av = sum(sum(sum(kappa_av)))...
        /length(x_un)/length(y_un)/length(z_un);
end
f0 = @(x)( x/sinh(x) - (2/3)*mu/(mu+kappa_av) );
f1 = @(x)( ( sinh(x)-x*cosh(x) )/(sinh(x))^2 );
root = newton( f0, f1, 1 );
C = (3/8)*root; A = ( tanh(0.5*root) )^(-1);
x_auto = @(x)(A*tanh(C*x.*(1+x.^2/3)));
x_auto_diff = @(x)(A*C*cosh(C*x.*(1+x.^2/3)).^(-2).*(1+x.^2));
y_auto = @(x)(x_auto(x)); y_auto_diff = @(x)(x_auto_diff(x));
xi1 = -1:2/(N+1):1; xi1_semi = -N/(N+1):2/(N+1):N/(N+1);
xi2 = -1:2/(K+1):1; xi2_semi = -K/(K+1):2/(K+1):K/(K+1);
x = 0.5*(bx+ax)*ones(1,N+2) + 0.5*(bx-ax)*x_auto(xi1);
y = 0.5*(by+ay)*ones(1,K+2) + 0.5*(by-ay)*y_auto(xi2);
hx = 0.5*(bx-ax)*x_auto_diff( xi1_semi )/(N+1);
hy = 0.5*(by-ay)*y_auto_diff( xi2_semi )/(K+1);
if( nargin == 3 )
    xi3 = -1:2/(L+1):1; xi3_semi = -L/(L+1):2/(L+1):L/(L+1);
    z_auto = @(x)(x_auto(x)); z_auto_diff = @(x)(x_auto_diff(x));
    z = 0.5*(bz+az)*ones(1,L+2) + 0.5*(bz-az)*z_auto(xi3);
    hz = 0.5*(bz-az)*z_auto_diff( xi3_semi )/(L+1);
end
end