function v = tridiagonal_matrix_algorithm(A,B,C,F,N)
% Performs three-diagonal matrix algorithm (aka run) for system with zero
% Dirichlet boundary conditions, given diagonals and right hand.
v = zeros(1,N+2); Alpha = v; Beta = v;
for n = 2:N+1
    Alpha(n+1) = B(n)/(C(n) - Alpha(n)*A(n));
    Beta(n+1)  = (F(n) + A(n)*Beta(n))/(C(n) - Alpha(n)*A(n));
end
v(N+2) = 0;
for r = 0:N
    v(N+1-r) = Alpha(N+2-r)*v(N+2-r) + Beta(N+2-r);            
end
end