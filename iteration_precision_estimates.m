function [precision_i,precision_e] = iteration_precision_estimates...
    ( u_all, epsilon_background )
% Calculates relaxation count accuracy estimates (interpolational and
% extrapolational ones) and compares them with round-off background
% estimates.
dim = size(u_all); I = dim(end);
precision_i = zeros(1,I-1); precision_e = zeros(1,I-2);
if( length(dim) == 3 )
    for log_grid_num = 1:I-1
        diff1 = u_all(:,:,log_grid_num+1) - u_all(:,:,log_grid_num);
        precision_i(log_grid_num) = norm_calc( diff1 );
    end
    for log_grid_num = 1:I-2
        diff2 = u_all(:,:,log_grid_num+1) - u_all(:,:, log_grid_num );
        diff3 = u_all(:,:,log_grid_num+2) - u_all(:,:,log_grid_num+1);
        Den = norm_calc( diff2 ); Num = norm_calc( diff3 );
        precision_e(log_grid_num) = Num^3/Den^2;
    end
end
if( length(dim) == 4 )
    for log_grid_num = 1:I-1
        diff1 = u_all(:,:,:,log_grid_num+1) - u_all(:,:,:,log_grid_num);
        precision_i(log_grid_num) = norm_calc( diff1 );
    end
    for log_grid_num = 1:I-2
        diff2 = u_all(:,:,:,log_grid_num+1) - u_all(:,:,:, log_grid_num );
        diff3 = u_all(:,:,:,log_grid_num+2) - u_all(:,:,:,log_grid_num+1);
        Den = norm_calc( diff2 ); Num = norm_calc( diff3 );
        precision_e(log_grid_num) = Num^3/Den^2;
    end
end
for j = 1:I-1 % ��������� � ������� ���� ������ ����������
    precision_i(j) = max( epsilon_background, precision_i(j) );
end
for j = 1:I-2
    precision_e(j) = max( epsilon_background, precision_e(j) );
end
end