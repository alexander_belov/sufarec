function R0 = richardson_multidim( u1, u2, p )
% Calculates point-wise estimation according to Richardson method.
dim = size(u1);
if( length(dim) == 2 )
    N = dim(1)-1; K = dim(2)-1; R0 = zeros(N+1,K+1);
    for n = 1:N+1
        for k = 1:K+1
            R0(n,k) = ( u2( 2*n-1,2*k-1 ) - u1( n,k ) )/( 2^p-1 );
        end
    end
end
if( length(dim) == 3 )
    N = dim(1)-1; K = dim(2)-1; L = dim(3)-1; R0 = zeros(N+1,K+1,L+1);
    for n = 1:N+1
        for k = 1:K+1
            for l = 1:L+1
                R0(n,k,l) = (u2(2*n-1,2*k-1,2*l-1) - u1(n,k,l))/( 2^p-1 );
            end
        end
    end
end
end