function pde_solve_illustration(u, N0, K0, L0, precision_output)
% Performs plots of the solution, its section and convergence for spatial
% meshes thickening.
global grid_choice;
dim = size(u);
if( length(dim) == 2 )
    N = dim(1) - 2; K = dim(2) - 2;
    if (grid_choice == 0)
        [x,~,y,~] = adaptive_mesh( N,K );
    else
        [x,~,y,~] = user_mesh( N,K );
    end
    figure; hold on; grid off; colormap(gray); colorbar;
    xlabel('x'); ylabel('y'); zlabel('u'); title('SOLUTION');
    [y1,x1] = meshgrid(y,x);
    surf(x1,y1,u); contour3(x1,y1,u,40,'k'); shading interp;
    if (N0 == K0)
        section = zeros(1,K+2); arg = section;
        for k = 1:K+2
            section(k) = u(k,k); 
            arg(k) = sqrt( (x(k)-min(x))^2+(y(k)-min(y))^2 );
        end
        figure; xlabel('sqrt(x^2 + y^2)'); ylabel('u'); title('SECTION');
        hold on; plot(arg,section,'-ok','MarkerEdgeColor','k',...
            'MarkerFaceColor','k','MarkerSize',4)
    end
    figure; xlabel('lg N'); ylabel('lg epsilon'); title('ACCURACY'); 
    temp = (N0*K0)^(1/2); NN = zeros(1,length(precision_output));
    for m = 1:length(precision_output)
        NN(m) = temp*2^(m);
    end
    hold on; plot(log10(NN),log10(precision_output),...
        '-ok','MarkerEdgeColor','k','MarkerFaceColor','k','MarkerSize',14)
end
if( length(dim) == 3 )
    N = dim(1) - 2; K = dim(2) - 2; L = dim(3) - 2;
    if (grid_choice == 0)
        [x,~,y,~] = adaptive_mesh( N,K );
    else
        [x,~,y,~] = user_mesh( N,K );
    end
    figure; hold on; grid off; colormap(gray); colorbar;
    xlabel('x'); ylabel('y'); zlabel('u'); title('SOLUTION,z = (bz+az)/2');
    [y1,x1] = meshgrid(y,x); fixed_z = u(:,:,ceil(L/2+1));
    surf(x1,y1,fixed_z); contour3(x1,y1,fixed_z,40,'k'); shading interp;
    if (N0 == K0)
        section = zeros(1,K+2); arg = section;
        for k = 1:K+2
            section(k) = fixed_z(k,k);
            arg(k) = sqrt( (x(k)-min(x))^2+(y(k)-min(y))^2 );
        end
        figure; xlabel('sqrt(x^2 + y^2)'); ylabel('u'); title('SECTION');
        hold on; plot(arg,section,'-ok','MarkerEdgeColor','k',...
            'MarkerFaceColor','k','MarkerSize',4)
    end
    figure; xlabel('lg N'); ylabel('lg epsilon'); title('ACCURACY');
    temp = (N0*K0*L0)^(1/2); NN = zeros(1,length(precision_output));
    for m = 1:length(precision_output)
        NN(m) = temp*2^(m);
    end
    hold on; plot(log10(NN),log10(precision_output),...
        '-ok','MarkerEdgeColor','k','MarkerFaceColor','k','MarkerSize',14)
end
end