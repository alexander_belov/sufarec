function tau = logarithmic_set( lambda_max, lambda_min, S )
% Constructs linear-trigonometric set in logarithmic scale with given
% number of steps.
if( length(lambda_max) == 2 )
    tau_min = 2/sum(lambda_max); tau_max = 2/sum(lambda_min);
end
if( length(lambda_max) == 3 )
    l_x = lambda_min(1); l_y = lambda_min(2); l_z = lambda_min(3);
    a = l_x + l_y + l_z; b = l_x*l_y + l_x*l_z + l_y*l_z; c = l_x*l_y*l_z;
    phi = real( (1/3)*acos( - c * (b/3)^(-1.5 ) ) );
    tau_star = - sqrt(3)/( sqrt(b) * cos(phi+2*pi/3) );
    fr = 2/tau_star;
    Num = fr^3 - a*fr^2 + b*fr + c; Den = fr^3 + a*fr^2 + b*fr + c;
    rho = Num/Den;
    if( rho > 0 ); tau_max = tau_star; end
    if( rho < 0 )
        kard1 = (a^2)/3 - b; kard2 = -2*(a^3)/27 + a*b/3 + c;
        phi = real( (1/3)*acos( (kard2/2)*(kard1/3)^(-1.5 ) ) );
        fr = -2*sqrt(kard1 /3)*cos( phi - 2*pi/3 );
        tau_max = 2/( fr + a/3 );
    end
    l_x = lambda_max(1); l_y = lambda_max(2); l_z = lambda_max(3);
    a = l_x + l_y + l_z; b = l_x*l_y + l_x*l_z + l_y*l_z; c = l_x*l_y*l_z;
    phi = real( (1/3)*acos( - c*(b/3)^(-1.5 ) ) );
    tau_star = - sqrt(3)/( sqrt(b)*cos(phi+2*pi/3) );
    fr = 2/tau_star;
    Num = fr^3 - a*fr^2 + b*fr + c; Den = fr^3 + a*fr^2 + b*fr + c;
    rho = Num/Den;
    if( rho > 0 ); tau_min = tau_star; end
    if( rho < 0 )
        kard1 = (a^2)/3 - b; kard2 = -2*(a^3)/27 + a*b/3 + c;
        phi = real( (1/3)*acos( (kard2/2)*(kard1/3)^(-1.5 ) ) );
        fr = -2*sqrt(kard1 /3)*cos( phi + 2*pi/3 );
        tau_min = 2/( fr + a/3 );
    end
end
center = 0.5*log(tau_min*tau_max); width = 0.5*log(tau_max/tau_min);
tau = zeros(1,S+1); theta = tau; lt = tau;
for s = 1:S+1
    theta(s) = (s-1)/(S);
    lt(s) = (2/(pi+2))*cos(theta(s)*pi-pi) + (pi/(pi+2))*(2*theta(s)-1);
    tau(s) = exp( center + width*( lt(s) ) );
end
end